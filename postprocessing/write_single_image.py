import os
import numpy as np
import pandas as pd
import cv2

from utils.general import (plot_one_box)

def WriteImage(img_file,save_path,csv_file):
    imgtype = ['jpg','png','jpeg','JPG','PNG','JPEG']
    imgpath = img_file
    for i in imgtype:
        if os.path.exists(imgpath + i):
            imgpath = imgpath + i
            break
    print(imgpath)
    img = cv2.imread(imgpath)
    csv = pd.read_csv(csv_file,encoding='utf-16')
    count = 0
    for index,i in csv.iterrows():
        x1 = int(i['x'] - i['w']/2)
        x2 = int(i['x'] + i['w']/2)
        y1 = int(i['y'] - i['h']/2)
        y2 = int(i['y'] + i['h']/2) 
        label = '%s' % (count) 
        xyxy=[x1,y1,x2,y2]
        plot_one_box(xyxy, img, label=label, color=(255,0,0), line_thickness=2)        
        count+=1
    cv2.imwrite(save_path,img)
        
