import numpy as np
import pandas as pd
import argparse
from sklearn.preprocessing import LabelEncoder
import os
import platform
import shutil
import time
from pathlib import Path
import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random
from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import (
    check_img_size, non_max_suppression, apply_classifier, scale_coords,
    xyxy2xywh, plot_one_box, strip_optimizer, set_logging)
from utils.torch_utils import select_device, load_classifier, time_synchronized
from keras.models import Model, load_model

from preproces_input import (
        preprocess_input,
        GlobalWeightedAveragePooling2D
        )

def Preprocessing(images):
  #ims = []
  #for image in images:
  #  ims.append(preprocess_input(images,False))
  #return ims
  return preprocess_input(images,False)

def load_detector(weights):
    device = select_device('')
    model = attempt_load(weights, map_location=device)  # load FP32 model
    return model

def load_classifier():
    classifier_model =  load_model(MODEL_PATH, custom_objects={"GlobalWeightedAveragePooling2D": GlobalWeightedAveragePooling2D})
    return classifier_model
'''
DETECT FUNCTION
'''
FILEPATH = __file__.replace("segmentation.py","")
MODEL_PATH = os.path.join(FILEPATH,'weights/model.47-16.17.hdf5')

def detect(save_img=True , output_path = "./output", source_path="", 
weight="yolov5NomOCR_last_2.pt", img_size=480, 
predict_th = 0.5,useThres=False,grayscale=False,
load_model=True, detector=None, classifier=None
):
    out, source, weights, view_img, save_txt, imgsz, predict_threshold, useThres, grayscale, load_model, detector, classifier = \
        output_path, source_path, weight, False, True, img_size, predict_th, useThres, grayscale, load_model,detector, classifier
    webcam = source.isnumeric() or source.startswith(('rtsp://', 'rtmp://', 'http://')) or source.endswith('.txt')
    previous_label = ""
    nom_char_code = pd.read_csv(os.path.join(FILEPATH,'labels/Nom_character_code.csv'), encoding='utf-16',index_col='Unicode')
    nom_char_code.loc[:, 'Code'] = nom_char_code.Code.str.replace('\[.*\]', '')
    # Initialize
    label_encoder = LabelEncoder()
    label_encoder.classes_ = np.load(os.path.join(FILEPATH,'labels/label2char.npy'))
    classifier_model = None
    if load_model:
        classifier_model =  load_model(MODEL_PATH, custom_objects={"GlobalWeightedAveragePooling2D": GlobalWeightedAveragePooling2D})
    else:
        classifier_model = classifier
    set_logging()
 
    device = select_device('')
    if not os.path.exists(out):
        os.makedirs(out)  # make new output folder
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    if load_model:
        model = attempt_load(weights, map_location=device)  # load FP32 model
        if half:
            model.half()  # to FP16
    else:
        model = detector
    imgsz = check_img_size(imgsz, s=model.stride.max())  # check img_size
    # Second-stage classifier
    classify = False
    if classify:
        modelc = load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(torch.load('weights/resnet101.pt', map_location=device)['model'])  # load weights
        modelc.to(device).eval()
        
    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = True
        cudnn.benchmark = True  # set True to speed up constant image size inference
        dataset = LoadStreams(source, img_size=imgsz)
    else:
        save_img = True
        dataset = LoadImages(source, img_size=imgsz,grayscale=grayscale)

    # Get names and colors
    names = model.module.names if hasattr(model, 'module') else model.names
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(label_encoder.classes_))]
    print("Colors", len(colors))
    # Run inference
    t0 = time.time()
    img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
    _ = model(img.half() if half else img) if device.type != 'cpu' else None  # run once
    for path, img, im0s, vid_cap in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        t1 = time_synchronized()
        pred = model(img, augment=False)[0]

        # Apply NMS
        pred = non_max_suppression(pred, 0.4, 0.5, classes=None, agnostic=False)
        t2 = time_synchronized()

        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)

        # Process detections
       
        for i, det in enumerate(pred):  # detections per image
            if webcam:  # batch_size >= 1
                p, s, im0 = path[i], '%g: ' % i, im0s[i].copy()
            else:
                p, s, im0 = path, '', im0s

            save_path = str(Path(out) / Path(p).name)
            txt_path = str(Path(out) / Path(p).stem) + ('_%g' % dataset.frame if dataset.mode == 'video' else '')
            s += '%gx%g ' % img.shape[2:]  # print string
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
            im1 = np.copy(im0)
            #with open(txt_path +'.csv', 'w', encoding='utf-16') as f: 
            #    f.write('index, top1, top2, top3, top4, top5, x, y, w, h \n')
            results = pd.DataFrame(columns=['id','x', 'y', 'w', 'h', 'unicode1', 'unicode2', 'unicode3', 'unicode4', 'unicode5', 'top1', 'top2', 'top3', 'top4', 'top5', 'conf1', 'conf2', 'conf3', 'conf4', 'conf5'])
            if det is not None and len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += '%g %ss, ' % (n, names[int(c)])  # add to string
                # Write results
                count = 0
                for *xyxy, conf, cls in reversed(det):
                    if True:  # Write to file
                        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                        frame = torch.tensor(xyxy).tolist()
                        x1,x2,y1,y2 = int(frame[1]),int(frame[3]), int(frame[0]), int(frame[2])
                        #print(im0.shape)
                        #print(x1,y1,x2,y2)
                        crop_img = im0[x1:x2,y1:y2]
                        feature = Preprocessing(crop_img)
                        predictions = classifier_model.predict(feature)
                        prediction = np.argsort(predictions)[0][:-6:-1]                        #print(predictions)
                        confs = np.sort(predictions)[0][:-6:-1]
                        #print(confs)
                        #print(confs[0])
                        if (not useThres) or predictions[0][prediction[0]] >= predict_threshold:
                            result = pd.DataFrame([(count, xywh[0], xywh[1], xywh[2], xywh[3],
                                label_encoder.inverse_transform(prediction)[0],
                                label_encoder.inverse_transform(prediction)[1],
                                label_encoder.inverse_transform(prediction)[2],
                                label_encoder.inverse_transform(prediction)[3],
                                label_encoder.inverse_transform(prediction)[4],
                                nom_char_code.loc[label_encoder.inverse_transform(prediction),'Character'].tolist()[0],
                                nom_char_code.loc[label_encoder.inverse_transform(prediction),'Character'].tolist()[1],
                                nom_char_code.loc[label_encoder.inverse_transform(prediction),'Character'].tolist()[2],
                                nom_char_code.loc[label_encoder.inverse_transform(prediction),'Character'].tolist()[3],
                                nom_char_code.loc[label_encoder.inverse_transform(prediction),'Character'].tolist()[4],
                                confs[0],confs[1],confs[2],confs[3],confs[4])],columns=['id','x', 'y', 'w', 'h',  'unicode1', 'unicode2', 'unicode3', 'unicode4', 'unicode5', 'top1', 'top2', 'top3', 'top4', 'top5', 'conf1', 'conf2', 'conf3', 'conf4', 'conf5'])

                            results = results.append(result,ignore_index=True)

                            if save_img:
                                label = str(count)
                                plot_one_box(xyxy, im1, label=label, color=colors[prediction[0]], line_thickness=1)
                    
                        else:
                        
                            result = pd.DataFrame([(count, xywh[0], xywh[1], xywh[2], xywh[3],"UKN","UKN","UKN","UKN","UKN",0,0,0,0,0)],columns=['id','x', 'y', 'w', 'h','unicode1', 'unicode2', 'unicode3', 'unicode4', 'unicode5',  'top1', 'top2', 'top3', 'top4', 'top5',  'conf1', 'conf2', 'conf3', 'conf4', 'conf5'])
                            results = results.append(result,ignore_index=True)

                            if save_img:
                                plot_one_box(xyxy, im1, label=str(count), color=colors[prediction[0]], line_thickness=1)
                        count+=1
                    
                        
            # Print time (inference + NMS)
            print('ok')
            print(results)
            results.to_csv(txt_path +'.csv',encoding='utf-16',index=False)   
            # Stream results
            if view_img:
                cv2.imshow(p, im0)
                if cv2.waitKey(1) == ord('q'):  # q to quit
                    raise StopIteration

            # Save results (image with detections)
            #cv2.imwrite(save_path, im1)
    if save_txt or save_img:
        if platform.system() == 'Darwin' and not False:  # MacOS
            os.system('open ' + save_path)
