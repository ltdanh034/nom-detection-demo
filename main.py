import argparse
import os 
from segmentation import detect
#from remove_sort_write import remove_sort_write
from postprocessing.to_pixel_position import ToPixelPosition
from postprocessing.write_single_image import WriteImage

def process(_weight, isize, threshold, dir_path, dir_out,useThres=False,grayscale=False,load_model=False,detector=None, classifier=None):
    ld = sorted(os.listdir(dir_path))
    for i in ld:
        detect(weight=_weight, output_path = dir_out,source_path=os.path.join(dir_path,i),img_size=int(isize),
            predict_th=float(threshold),useThres=useThres,
            grayscale=grayscale,load_model=load_model,
            detector=detector,classifier=classifier)
        if i == 'jpeg' or i == 'JPEG':
            ToPixelPosition(os.path.join(dir_out,i[:-4] + 'csv'), os.path.join(dir_path,i[:-4]),os.path.join(dir_out,i[:-4] + 'csv'))
            #WriteImage(os.path.join(dir_path,i[:-4]),os.path.join(dir_out,i[:-4]+ 'jpg'),os.path.join(dir_out,i[:-4] + 'csv'))
        else:
            ToPixelPosition(os.path.join(dir_out,i[:-3] + 'csv'), os.path.join(dir_path,i[:-3]),os.path.join(dir_out,i[:-3] + 'csv'))
            #WriteImage(os.path.join(dir_path,i[:-3]),os.path.join(dir_out,i[:-3]+ 'jpg'),os.path.join(dir_out,i[:-3] + 'csv'))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dir_path', type=str, help='Image directory')
    parser.add_argument('--weights', type=str, default='weights/yolov5NomOCR_last_2.pt', help='model.pt path(s)')
    parser.add_argument('--output', type=str, default='output', help='output folder')
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--thres', type=float, default=0.4, help='Detection confidence threshold')
    parser.add_argument('--predict-threshold', type=float, default=0.5, help='Identification confidence threshold')
    parser.add_argument('--use-thres',action="store_true",help='use predict threshold')
    parser.add_argument('--convert-grayscale',action='store_true', help='You have to convert color image to grayscale if character\'s colors are not black')
    opt = parser.parse_args()
    print(opt)
    process(opt.weights,opt.img_size,opt.thres,opt.dir_path,opt.output,opt.use_thres,opt.convert_grayscale)
    #remove_sort_write(opt.dir_path,opt.output,opt.output)

