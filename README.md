# Nom Recognition Demo
## Cài đặt
### Install dependencies
```
pip install -r requirements.txt
```
Chép weight của model vào folder /weights

## Các lưu ý trong phiên bản này
- Demo hiện tại dùng data sample với 566 label. Có thể dùng flag --use-thres để loại các chữ detect được nhưng độ tự tin khi identify thấp.

- Các hình ảnh có chứa ký tự khác màu đen thì cần thêm flag --convert-grayscale để đưa về grayscale. Trường hợp chữ màu sáng trên nền tối (trên các tấm bia) thì cần grayscale sau đó reverse lại màu (chưa xử lí tự động vì hiện tại chưa cần thực hiện trên loại data này).

- Do model hiện tại train trên data lấy từ Nôm Foundation website nên kích cỡ của các ký tự chứa trong 1 trang dùng để feed model với kích cỡ WxH trong đó W và H trong khoảng 25 tới 40. Nên các hình ảnh cần được resize đảm bảo các chữ nằm trong ảnh có giá kích cỡ trong khoảng này. *Sẽ được giải quyết trong tương lai nếu có thêm data các kích cỡ chữ khác nhau để feed cho model*.

## Các lỗi
### Không cài được pytorch 1.6.0 và torchvision 0.7.0
Do pip ở phiên bản python hiện tại không tìm thấy phiên bản phù hợp.
##### Solution
Sử dụng wheel để cài 
```
$ pip install torch==1.6.0+cpu torchvision==0.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
```
### Không cài được một số package do thiếu các dependencies của package đó hoặc sai phiên bản
##### Solution
Dùng 1 trong 2 lệnh sau để cài phiên bản cụ thể, hoặc để downgrade phiên bản của một package hiện có
```
$ pip install --upgrade [package]==[version]
$ python -m pip install --upgrade [package]==[version]
```
