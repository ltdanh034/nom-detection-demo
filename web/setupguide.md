# Set up guide
Python version: 3.8.7
## API
### 1. Segmentation weight import
i[Weight link, size 170MB](https://drive.google.com/file/d/1VozcSn3R-Fg0sh1OVk6A08xPO-MmOPQt/view?usp=sharing) 

i[Weight link 2](https://drive.google.com/file/d/16kx2jGqEgVBCUHZBleDdiUaE8RyOa-CV/view?usp=sharing) 

i[Weight link, small model](https://drive.google.com/file/d/1bF7cSylKzVVX9oMgyBmYdLzjrhk01oPn/view?usp=sharing) 

Copy exp8_last.pt -> web/restapi

### 2. Activate python virtual environment (venv)

```
$ cd nom-detection-demo
$ python -m venv venv
$ source venv/bin/activate
$ ./venv/bin/activatie
$ pip install -r requirements.txt
```

### 3. Run api.py
- Open api.py and modify PORTNUMBER if needed.
```
$ cd web/restapi
$ python api.py
```
## Client 
- Open index.html file in folder Client, modify the port number if needed.
