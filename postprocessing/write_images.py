from to_pixel_position import ToPixelPosition
from write_single_image import WriteImage
import os

def WriteImages(img_path, csv_path, output_path):
    list_csv = sorted(os.listdir(csv_path))
    for i in list_csv:
        if i[-3:] != 'csv':
            continue
        ToPixelPosition(os.path.join(csv_path,i),os.path.join(img_path,i[:-3]),os.path.join(output_path, i))
    #for i in list_csv:        
    #    if i[-3:] == 'csv':      
    #        WriteImage(os.path.join(img_path,i[:-3]),os.path.join(output_path,i[:-3]+ 'jpg'),os.path.join(output_path,i))

