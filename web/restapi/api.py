import os
from flask import Flask, send_from_directory
from flask_restful import Resource, Api, reqparse
from werkzeug.utils import secure_filename
import sys
from werkzeug.datastructures import FileStorage, MultiDict
sys.path.append("../../")
from segmentation import detect, load_detector, load_classifier
#from postprocessing.to_pixel_position import ToPixelPosition
#from postprocessing.write_single_image import WriteImage
from main import process
import random
import zipfile
import string 

PORTNUMBER = 8000

UPLOAD_DIRECTORY = "tmp"

if not os.path.exists(UPLOAD_DIRECTORY):
    os.makedirs
app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('image_data', type=FileStorage, action='append',location='files',)
parser.add_argument('image_size', type=int, default=640)
parser.add_argument('filename',type=str)
parser.add_argument('threshold', type=float, default=0.4)
parser.add_argument('use_threshold', type=bool)
parser.add_argument('grayscale',type=bool)

detector = load_detector('small_a_l_2.pt')
classifier = load_classifier()

def zipdir(path, ziph):
    print("Trying to compress folder")
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            if file[-3:] != 'zip':
                ziph.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), os.path.join(path, '..')))
  
def id_generator(size=24, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def handle_file(f,filepath):
    filename = secure_filename(f.filename)
    f.save(os.path.join(filepath, filename))

class UploadImages(Resource):
    def post(self):
        args = parser.parse_args() 
        print(args)
        #filename = args['filename']
        image_file = args['image_data']
        token = id_generator()
        images_path = 'images' + str(token)
        filename=secure_filename(image_file.filename)
        print(filename)
        if not os.path.exists(os.path.join(UPLOAD_DIRECTORY,images_path)):
            os.makedirs(os.path.join(UPLOAD_DIRECTORY,images_path))
        
        image_file.save(os.path.join(UPLOAD_DIRECTORY,images_path,filename))
        
        return filename + ' was uploaded successful.', 201

class Predict(Resource):
    def get(self):
        args = parser.parse_args()
        image_size = args['image_size']
        threshold = args['threshold']
        use_threshold = args['use_threshold']
        grayscale = args['grayscale']
        #token = args['token']
        images_path = 'images' + token
        input_path = os.path.join(UPLOAD_DIRECTORY,images_path)
        output_path = os.path.join(UPLOAD_DIRECTORY, 'output' + token) 
        if not os.path.exists(os.path.join(UPLOAD_DIRECTORY,images_path)):
            os.makedirs(os.path.join(UPLOAD_DIRECTORY,images_path)) 
        if not os.path.exists(os.path.join(UPLOAD_DIRECTORY,'output' + token)):
            os.makedirs(os.path.join(UPLOAD_DIRECTORY,'output' + token)) 
        process('exp8_last.pt',image_size, threshold, input_path, output_path, use_threshold, grayscale,False, detector, classifier)
        return "Prediction completed.", 201

class Download(Resource):
    def get(self):
        args = parser.parse_args()
        #path = args['downloadpath']
        token = args['token']
        output_path = os.path.join(UPLOAD_DIRECTORY, 'output' + token) 
        zipf = zipfile.ZipFile(os.path.join(output_path,'out.zip'), 'w', zipfile.ZIP_DEFLATED)
        zipdir(output_path, zipf)
        zipf.close()
        print("Trying to send zip file")
        return send_from_directory(output_path,'out.zip',as_attachment=True)

class Process(Resource):
    def post(self):
        args = parser.parse_args() 
        print(args)
        #filename = args['filename']
        files = args['image_data']
        image_size = args['image_size']
        threshold = args['threshold']
        use_threshold = args['use_threshold']
        grayscale = args['grayscale']
        token = id_generator()
        images_path = 'images' + str(token)
        if not os.path.exists(os.path.join(UPLOAD_DIRECTORY,images_path)):
            os.makedirs(os.path.join(UPLOAD_DIRECTORY,images_path))
        #image_file.save(os.path.join(UPLOAD_DIRECTORY,images_path,filename))
        input_path = os.path.join(UPLOAD_DIRECTORY,images_path)
        for file in files:
            handle_file (file, input_path)
        output_path = os.path.join(UPLOAD_DIRECTORY, 'output' + token)
        process('exp8_last.pt',image_size, threshold, input_path, output_path, use_threshold, grayscale,False,detector,classifier)
        zipf = zipfile.ZipFile(os.path.join(output_path,'out.zip'), 'w', zipfile.ZIP_DEFLATED)
        zipdir(output_path, zipf)
        zipf.close()
        return send_from_directory(output_path,'out.zip',as_attachment=True)

api.add_resource(Process,'/api/process')
#api.add_resource(UploadImages, '/api/upload')
#api.add_resource(Predict, '/api/predict')
#api.add_resource(Download, '/api/download')
if __name__ == '__main__':
    app.run(debug=True,port=PORTNUMBER)
