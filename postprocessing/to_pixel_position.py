import os
import pandas as pd
import numpy as np
import cv2

th = 10

def ToPixelPosition(csv_file, img_file, sorted_csv):
    imgtype = ['jpg','png','jpeg','JPG','PNG','JPEG']
    imgpath = img_file
    for i in imgtype:
        if os.path.exists(imgpath + i):
            imgpath = imgpath + i
            break
    print(imgpath)
    img = cv2.imread(imgpath)
    w = img.shape[1]
    h = img.shape[0]
    csv = pd.read_csv(csv_file,encoding='utf-16')
    #print(csv)
    csv.columns = ['index','x', 'y', 'w', 'h','unicode1','unicode2','unicode3','unicode4','unicode5','top1','top2','top3','top4','top5','conf1', 'conf2', 'conf3', 'conf4', 'conf5']
    csv['w'] = csv['w']*w
    csv['h'] = csv['h']*h
    csv['x'] = csv['x']*w
    csv['y'] = csv['y']*h
    csv['w'] = csv['w'].astype('int')
    csv['h'] = csv['h'].astype('int')
    csv['x'] = csv['x'].astype('int') - (csv['w']/2).astype('int')
    csv['y'] = csv['y'].astype('int') - (csv['h']/2).astype('int')   
    csv['w'] = csv['x'].astype('int') + csv['w']
    csv['h'] = csv['y'].astype('int') + csv['h']

    A = csv['w']
    B = csv['h']
    meanA = np.median(A) * 1.4
    meanB = np.median(B) * 1.4
    csv=csv[csv['w'] < meanA]
    csv=csv[csv['h'] < meanB]
    csv=csv[meanA/2 < csv['w']]
    csv=csv[meanA/2 < csv['h']]
    
    csv=csv.sort_values(
            by = "x",
            ascending=False)
    columns = [0]
    count = 0
    position=None
    try:
        position = csv.iloc[0]['x']
    except:
        return
    for i in csv['x'][1:]:
        if abs(i-position) < th:
            columns.append(count)
        else:
            count+=1
            columns.append(count)
        position = i
    csv['col'] = columns
    csv = csv.sort_values(['col', 'y'], ascending=[True, True])
    csv=csv.drop(columns='col')
    index = range(len(csv['index']))
    csv['index'] = index
    print(csv)
    csv.columns = [
            'ID', 'LEFT', 'TOP', 'RIGHT', 'BOTTOM',
            'UNICODE1', 'UNICODE2', 'UNICODE3', 'UNICODE4', 'UNICODE5', 
            'CHAR1', 'CHAR2', 'CHAR3', 'CHAR4', 'CHAR5',
            'CONFIDENCE1', 'CONFIDENCE2', 'CONFIDENCE3', 'CONFIDENCE4', 'CONFIDENCE5'
            ]
    csv.to_excel(sorted_csv[:-3] + 'xlsx',encoding='utf-16',index=False)
    csv.to_csv(sorted_csv,encoding='utf-16',index=False)

